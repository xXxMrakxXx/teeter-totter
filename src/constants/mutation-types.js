export const TOGGLE_PAUSE = "TOGGLE_PAUSE";
export const TOGGLE_ALERT = "TOGGLE_ALERT";
export const ADD_RIGHT_SIDE_BLOCK = "ADD_RIGHT_SIDE_BLOCK";
export const ADD_LEFT_SIDE_BLOCK = "ADD_LEFT_SIDE_BLOCK";
export const ADD_FALLING_BLOCK = "ADD_FALLING_BLOCK";
export const INITIALIZE_FALLING_BLOCKS = "INITIALIZE_FALLING_BLOCKS";
export const RESET_STATE = "RESET_STATE";
export const FINISH_FALLING = "FINISH_FALLING";
export const START_NEW_GAME = "START_NEW_GAME";
export const MOVE_RIGHT = "MOVE_RIGHT";
export const MOVE_LEFT = "MOVE_LEFT";
