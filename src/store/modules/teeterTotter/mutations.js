import { FALLING_BLOCKS_COUNT } from "@/constants";
import {
  TOGGLE_PAUSE,
  TOGGLE_ALERT,
  ADD_RIGHT_SIDE_BLOCK,
  ADD_LEFT_SIDE_BLOCK,
  RESET_STATE,
  INITIALIZE_FALLING_BLOCKS,
  ADD_FALLING_BLOCK,
  MOVE_RIGHT,
  MOVE_LEFT,
} from "@/constants/mutation-types";

import { generateRandomBlock } from "@/helpers/randomize";

const mutations = {
  [TOGGLE_PAUSE](state) {
    state.isPaused = !state.isPaused;
  },
  [ADD_RIGHT_SIDE_BLOCK](state) {
    const randomBlock = generateRandomBlock();
    state.rightSideBlocks.push(randomBlock);
  },
  [ADD_LEFT_SIDE_BLOCK](state) {
    const block = state.fallingBlocks.shift();
    state.leftSideBlocks.push(block);
  },
  [INITIALIZE_FALLING_BLOCKS](state) {
    state.fallingBlocks = [];
    for (let i = 0; i < FALLING_BLOCKS_COUNT; i++) {
      const randomBlock = generateRandomBlock();
      state.fallingBlocks.push(randomBlock);
    }
  },
  [ADD_FALLING_BLOCK](state) {
    const randomBlock = generateRandomBlock();
    state.fallingBlocks.push(randomBlock);
  },
  [MOVE_RIGHT](state) {
    if (state.isPaused || state.fallingBlocks[0].offset - 1 <= 0) return;
    state.fallingBlocks[0].offset -= 1;
  },
  [MOVE_LEFT](state) {
    if (state.isPaused || state.fallingBlocks[0].offset + 1 > 5) return;
    state.fallingBlocks[0].offset += 1;
  },
  [RESET_STATE](state) {
    state.isPaused = true;
    state.leftSideBlocks = [];
    state.rightSideBlocks = [];
    state.fallingBlocks = [];
  },
  [TOGGLE_ALERT](state, value) {
    state.alertStatus = value;
  },
};

export default mutations;
