const state = () => ({
  isPaused: true,
  leftSideBlocks: [],
  rightSideBlocks: [],
  fallingBlocks: [],
  alertStatus: false,
});

export default state;
