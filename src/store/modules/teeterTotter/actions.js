import {
  ADD_FALLING_BLOCK,
  INITIALIZE_FALLING_BLOCKS,
  FINISH_FALLING,
  START_NEW_GAME,
  ADD_RIGHT_SIDE_BLOCK,
  ADD_LEFT_SIDE_BLOCK,
  RESET_STATE,
  TOGGLE_ALERT,
} from "@/constants/mutation-types";

const actions = {
  [FINISH_FALLING]({ commit, state, getters, dispatch }) {
    commit(ADD_LEFT_SIDE_BLOCK);
    commit(ADD_FALLING_BLOCK);

    if (state.leftSideBlocks.length && state.leftSideBlocks.length !== state.rightSideBlocks.length) {
      commit(ADD_RIGHT_SIDE_BLOCK);
    }

    if (getters.gameOverStatus) {
      setTimeout(() => {
        commit(TOGGLE_ALERT, true);
        dispatch(START_NEW_GAME);
      }, 0);
    }
  },
  [START_NEW_GAME]({ commit }) {
    commit(RESET_STATE);
    commit(ADD_RIGHT_SIDE_BLOCK);
    commit(INITIALIZE_FALLING_BLOCKS);
  },
};

export default actions;
