import { createStore } from "vuex";
import teeterTotter from "./modules/teeterTotter/index";

export default createStore({
  modules: {
    teeter: teeterTotter,
  },
});
